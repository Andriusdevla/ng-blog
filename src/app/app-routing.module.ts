import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogPageComponent } from './pages/blog-page/blog-page.component';

const routes: Routes = [
  { path: '', component: BlogPageComponent, pathMatch: 'full'},
  { path: 'post/:name', loadChildren: './pages/post-inner/post-inner.module#PostInnerModule'},
  { path: 'edit/:name/:index', loadChildren: './pages/edit/edit.module#EditModule'},
  { path: 'post', loadChildren: './pages/add-post/add-post.module#AddPostModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
