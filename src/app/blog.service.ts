import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './models/posts';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  postsUrl: string = "assets/data/posts.json";
  activeBlogPost : {};

  constructor(private http: HttpClient) {
   }

   public getPosts(): Observable<Posts[]>
   {
     return this.http.get<Posts[]>(this.postsUrl);
   }
}
