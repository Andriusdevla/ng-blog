import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MaterialModule } from '../material/material.module';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { SortPipe } from '../sort.pipe';

@NgModule({
  declarations: [
    ToolbarComponent,
    ListComponent,
    ItemComponent,
    SortPipe
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    ToolbarComponent,
    ListComponent,
    ItemComponent,
  ]
})
export class ComponentsModule { }
