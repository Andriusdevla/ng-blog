import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BlogService } from 'src/app/blog.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item;
  @Input() index;
  constructor(private router: Router, private blogService: BlogService) { }

  ngOnInit() {
  }

  preview(item){
    this.blogService.activeBlogPost = item;
    this.router.navigate(['/post', item.title]);
  }

  edit(item, i){
    this.blogService.activeBlogPost = item;
    this.router.navigate(['/edit', item.title, this.index]);
  }

}
