import { Component, OnInit, Input } from '@angular/core';
import { PageEvent } from '@angular/material';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() posts;
  sliceStart: number = 0;
  sliceEnd: number = 8;

  pageEvent : PageEvent;

  constructor() { }

  ngOnInit() {
  }

 changePage(event){
   if(event.previousPageIndex < event.pageIndex){
    console.log('will go next');
    this.sliceStart = this.sliceStart + 8;
    console.log(this.sliceStart);
    this.sliceEnd = this.sliceEnd + 8;
     console.log(this.sliceEnd);
   }
    else {
      this.sliceStart = this.sliceStart - 8;
      console.log(this.sliceStart);
      this.sliceEnd = this.sliceEnd - 8;
      console.log(this.sliceEnd);
   }
 }

  sortByYear(){
   this.posts.sort((a:any, b:any) => {
     const year = a['date'].substring(0, 4);
     const year2 = b['date'].substring(0,4);
    if(year > year2){
      return -1;
    } else if (year < year2) {
      return 1;
    } else {
      return 0;
    }
   });
  }

  sortByMonth(){
    this.posts.sort((a:any, b:any) => {
      const month = a['date'].substring(5, 7);
      const month2 = b['date'].substring(5,7);
      console.log(month, month2)
     if(month > month2){
       return -1;
     } else if (month < month2) {
       return 1;
     } else {
       return 0;
     }
    });
   }

   sortByDay(){
    this.posts.sort((a:any, b:any) => {
      const day = a['date'].substring(8, 10);
      const day2 = b['date'].substring(8,10);
      console.log(day, day2)
     if(day > day2){
       return -1;
     } else if (day < day2) {
       return 1;
     } else {
       return 0;
     }
    });
   }

}
