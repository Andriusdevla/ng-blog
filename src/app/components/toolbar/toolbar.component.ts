import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(public router: Router, private _location: Location) { }

  ngOnInit() {
  }

  goBack(){
    this._location.back();
  }

  goToAdd(){
    // Dont want to create a new page so I am using the same with a little workaround
    this.router.navigate(['/post'])
  }
}
