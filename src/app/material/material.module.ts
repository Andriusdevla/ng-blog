
import { NgModule } from '@angular/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
@NgModule({
  imports: [MatPaginatorModule, MatInputModule, MatFormFieldModule, MatCardModule, MatToolbarModule, MatButtonToggleModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule ,MatStepperModule, MatRadioModule, MatButtonModule, MatIconModule, MatSlideToggleModule, MatProgressBarModule, MatProgressSpinnerModule, MatCheckboxModule],
  exports: [MatPaginatorModule, MatInputModule, MatFormFieldModule, MatCardModule, MatToolbarModule, MatButtonToggleModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule, MatStepperModule, MatRadioModule, MatButtonModule, MatIconModule, MatSlideToggleModule, MatProgressBarModule, MatProgressSpinnerModule, MatCheckboxModule],
})
export class MaterialModule { }