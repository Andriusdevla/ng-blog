export class Posts {
 
    public title: string;
    public img: string;
    public date: Date;
    public text: string;
   
    constructor(title:string, img: string, date: Date, text: string) {
      this.title = title;
      this.img = img;
      this.date = date;
      this.text = text
    }
     
  }