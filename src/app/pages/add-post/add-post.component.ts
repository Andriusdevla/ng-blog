import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BlogService } from 'src/app/blog.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['../edit/edit.component.scss']
})
export class AddPostComponent implements OnInit {
  item = {};
  index: any;
  newPost: boolean = false;

  postForm = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(10)]),
    text: new FormControl('', [Validators.required, Validators.minLength(125)]),
    date: new FormControl(''),
  });

  constructor(private blogService: BlogService, private router: Router,
    private route: ActivatedRoute) {
  }


  get form() { return this.postForm.controls; }

  ngOnInit() {
  }

  clearValue(value) {
    this.postForm.get(value).reset();
  }

  getEditDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();

    return yyyy + '/' + mm + '/' + dd;
  }

  save() {
    console.log(this.postForm)
    if(!this.postForm.invalid){
      console.log('nera erroru');
      this.postForm.get('date').setValue(this.getEditDate());
      if (typeof window !== 'undefined') {
        const localArray = JSON.parse(localStorage.getItem('posts'));
        const localItem = this.postForm['value'];
        localArray.push(localItem);
        localStorage.setItem('posts', JSON.stringify(localArray));
        this.router.navigate(['/']);
      }
    } else {
      console.log('yra erroru')
    }

  }

  delete(){
    this.postForm.get('date').setValue(this.getEditDate());
    if (typeof window !== 'undefined') {
      const localArray = JSON.parse(localStorage.getItem('posts'));
      const localItem = this.postForm['value'];
      if (this.index > -1) {
        localArray.splice(this.index, 1);
      }
      localStorage.setItem('posts', JSON.stringify(localArray));
      this.router.navigate(['/']);
    }
  }
}
