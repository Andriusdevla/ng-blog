import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from 'src/app/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { AddPostComponent } from './add-post.component';

@NgModule({
  declarations: [AddPostComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: AddPostComponent, pathMatch: 'full'}
    ]),
    ComponentsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AddPostModule {

}
