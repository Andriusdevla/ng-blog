import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogService } from 'src/app/blog.service';
import { Posts } from 'src/app/models/posts';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.scss']
})
export class BlogPageComponent implements OnInit, OnDestroy {
  posts = new Array<Posts>();
  subscription: Subscription;
  constructor(private blogService: BlogService) {
    this.getDataFromLocalStorage();
   }

  getDataFromLocalStorage(){
    if(typeof window !== 'undefined'){
      if(localStorage.getItem('posts') != undefined){
      this.posts = JSON.parse(localStorage.getItem('posts'));
      }
      else {
        this.getData();
      }
    }
  }

   getData(){
    this.subscription = this.blogService.getPosts().pipe(first()).subscribe(data => {
      this.posts = data;
      console.log(this.posts)
     },
     (error) => {
       console.log(error);
     },
     () => {
       if(typeof window !== 'undefined'){
         localStorage.setItem('posts', JSON.stringify(this.posts));
       }
     })
   }


  ngOnInit() {
  }

  ngOnDestroy(){
    if(this.subscription != undefined){
      console.log('unsubscribing')
      this.subscription.unsubscribe();
    }
  }


}
