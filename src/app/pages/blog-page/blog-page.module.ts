import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { BlogPageComponent } from './blog-page.component';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  declarations: [BlogPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: BlogPageComponent, pathMatch: 'full'}
    ]),
    ComponentsModule
  ]
})
export class BlogPageModule {

}
