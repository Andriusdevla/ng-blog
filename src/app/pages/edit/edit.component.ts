import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/blog.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  item = {};
  index: any;
  newPost: boolean = false;

  postForm = new FormGroup({
    img: new FormControl(''),
    category: new FormControl(''),
    title: new FormControl('', [Validators.required, Validators.minLength(10)]),
    text: new FormControl('', [Validators.required, Validators.minLength(125)]),
    date: new FormControl('', [Validators.required]),
  });

  constructor(private blogService: BlogService, private router: Router,
    private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.index = params['index'];
    }
    )
    // Because I don't have an ID of a post, I am calling it from a service
    if (this.blogService.activeBlogPost !== undefined) {
      this.item = this.blogService.activeBlogPost;
    } else {
      this.router.navigate(['/']);
    }
  }


  get form() { return this.postForm.controls; }

  ngOnInit() {
    this.postForm.patchValue(this.item);
  }

  clearValue(value) {
    this.postForm.get(value).reset();
  }

  getEditDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();

    return yyyy + '/' + mm + '/' + dd;
  }

  save() {
    if(!this.postForm.invalid){
      console.log('nera erroru');
      this.postForm.get('date').setValue(this.getEditDate());
      if (typeof window !== 'undefined') {
        const localArray = JSON.parse(localStorage.getItem('posts'));
        const localItem = this.postForm['value'];
        if (this.index > -1) {
          localArray.splice(this.index, 1);
        }
        localArray.push(localItem);
        localStorage.setItem('posts', JSON.stringify(localArray));
        this.router.navigate(['/']);
      }
    } else {
      console.log('yra erroru')
    }

  }

  delete(){
    this.postForm.get('date').setValue(this.getEditDate());
    if (typeof window !== 'undefined') {
      const localArray = JSON.parse(localStorage.getItem('posts'));
      const localItem = this.postForm['value'];
      if (this.index > -1) {
        localArray.splice(this.index, 1);
      }
      localStorage.setItem('posts', JSON.stringify(localArray));
      this.router.navigate(['/']);
    }
  }
}
