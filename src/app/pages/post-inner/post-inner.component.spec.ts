import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostInnerComponent } from './post-inner.component';

describe('PostInnerComponent', () => {
  let component: PostInnerComponent;
  let fixture: ComponentFixture<PostInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
