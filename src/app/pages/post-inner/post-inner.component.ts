import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-inner',
  templateUrl: './post-inner.component.html',
  styleUrls: ['./post-inner.component.scss']
})
export class PostInnerComponent implements OnInit {
  item = {};

  constructor(private blogService: BlogService, private router: Router) {
   // Because I don't have an ID of a post, I am calling it from a service
    if(this.blogService.activeBlogPost !== undefined){
      this.item = this.blogService.activeBlogPost;
    } else {
      this.router.navigate(['/']);
    }
   }

  ngOnInit() {
  }

}
