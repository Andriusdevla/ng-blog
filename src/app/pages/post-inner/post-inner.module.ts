import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from 'src/app/components/components.module';
import { PostInnerComponent } from './post-inner.component';

@NgModule({
  declarations: [PostInnerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PostInnerComponent, pathMatch: 'full'}
    ]),
    ComponentsModule
  ]
})
export class PostInnerModule {

}
